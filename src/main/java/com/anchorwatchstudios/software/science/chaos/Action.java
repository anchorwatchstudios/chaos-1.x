package com.anchorwatchstudios.software.science.chaos;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;

import com.anchorwatchstudios.software.science.chaos.fractal.Type;

public class Action {
	
	public static class New extends AbstractAction {
		
		private static final long serialVersionUID = 1L;
		private Type type;
		
        public New(String name, String description, ImageIcon icon, int mnemonic, Type type) {
        	
            super(name, icon);
            putValue(SHORT_DESCRIPTION, description);
            putValue(MNEMONIC_KEY, mnemonic);
            this.type = type;
        
        }

        @Override
        public void actionPerformed(ActionEvent event) {
        	
            Chaos.newFractal(type);
        
        }
    
	}
	
	public static class Open extends AbstractAction {
		
		private static final long serialVersionUID = 1L;
		
        public Open(String name, String description, ImageIcon icon, int mnemonic) {
        	
            super(name, icon);
            putValue(SHORT_DESCRIPTION, description);
            putValue(MNEMONIC_KEY, mnemonic);
        
        }

        @Override
        public void actionPerformed(ActionEvent event) {
        	
            Chaos.openFractal();
        
        }
    
	}
	
	public static class Save extends AbstractAction {
		
		private static final long serialVersionUID = 1L;
		
        public Save(String name, String description, ImageIcon icon, int mnemonic) {
        	
            super(name, icon);
            putValue(SHORT_DESCRIPTION, description);
            putValue(MNEMONIC_KEY, mnemonic);
        
        }

        @Override
        public void actionPerformed(ActionEvent event) {
        	
            Chaos.saveFractal(false);
        
        }
    
	}
	
	public static class SaveAs extends AbstractAction {
		
		private static final long serialVersionUID = 1L;
		
        public SaveAs(String name, String description, ImageIcon icon, int mnemonic) {
        	
            super(name, icon);
            putValue(SHORT_DESCRIPTION, description);
            putValue(MNEMONIC_KEY, mnemonic);
        
        }

        @Override
        public void actionPerformed(ActionEvent event) {
        	
            Chaos.saveFractal(true);
        
        }
    
	}
	
	public static class ZoomIn extends AbstractAction {
		
		private static final long serialVersionUID = 1L;
		
        public ZoomIn(String name, String description, ImageIcon icon, int mnemonic) {
        	
            super(name, icon);
            putValue(SHORT_DESCRIPTION, description);
            putValue(MNEMONIC_KEY, mnemonic);
        
        }

        @Override
        public void actionPerformed(ActionEvent event) {
        	
            Chaos.zoomIn();
        
        }
    
	}
	
	public static class ZoomOut extends AbstractAction {
		
		private static final long serialVersionUID = 1L;
		
        public ZoomOut(String name, String description, ImageIcon icon, int mnemonic) {
        	
            super(name, icon);
            putValue(SHORT_DESCRIPTION, description);
            putValue(MNEMONIC_KEY, mnemonic);
        
        }

        @Override
        public void actionPerformed(ActionEvent event) {
        	
            Chaos.zoomOut();
        
        }
    
	}
	
	public static class MoveLeft extends AbstractAction {
		
		private static final long serialVersionUID = 1L;
		
        public MoveLeft(String name, String description, ImageIcon icon, int mnemonic) {
        	
            super(name, icon);
            putValue(SHORT_DESCRIPTION, description);
            putValue(MNEMONIC_KEY, mnemonic);
        
        }

        @Override
        public void actionPerformed(ActionEvent event) {
        	
        	Chaos.moveLeft();
        
        }
    
	}
	
	public static class MoveRight extends AbstractAction {
		
		private static final long serialVersionUID = 1L;
		
        public MoveRight(String name, String description, ImageIcon icon, int mnemonic) {
        	
            super(name, icon);
            putValue(SHORT_DESCRIPTION, description);
            putValue(MNEMONIC_KEY, mnemonic);
        
        }

        @Override
        public void actionPerformed(ActionEvent event) {
        	
            Chaos.moveRight();
        
        }
    
	}
	
	public static class MoveUp extends AbstractAction {
		
		private static final long serialVersionUID = 1L;
		
        public MoveUp(String name, String description, ImageIcon icon, int mnemonic) {
        	
            super(name, icon);
            putValue(SHORT_DESCRIPTION, description);
            putValue(MNEMONIC_KEY, mnemonic);
        
        }

        @Override
        public void actionPerformed(ActionEvent event) {
        	
        	Chaos.moveUp();
        
        }
    
	}
	
	public static class MoveDown extends AbstractAction {
		
		private static final long serialVersionUID = 1L;
		
        public MoveDown(String name, String description, ImageIcon icon, int mnemonic) {
        	
            super(name, icon);
            putValue(SHORT_DESCRIPTION, description);
            putValue(MNEMONIC_KEY, mnemonic);
        
        }

        @Override
        public void actionPerformed(ActionEvent event) {
        	
            Chaos.moveDown();
        
        }
    
	}
	
	public static class EditSettings extends AbstractAction {
		
		private static final long serialVersionUID = 1L;
		
        public EditSettings(String name, String description, ImageIcon icon, int mnemonic) {
        	
            super(name, icon);
            putValue(SHORT_DESCRIPTION, description);
            putValue(MNEMONIC_KEY, mnemonic);
        
        }

        @Override
        public void actionPerformed(ActionEvent event) {
        	
            Chaos.toggleSettingsVisibility();
        
        }
    
	}
	
	public static class ExportImage extends AbstractAction {
		
		private static final long serialVersionUID = 1L;
		
        public ExportImage(String name, String description, ImageIcon icon, int mnemonic) {
        	
            super(name, icon);
            putValue(SHORT_DESCRIPTION, description);
            putValue(MNEMONIC_KEY, mnemonic);
        
        }

        @Override
        public void actionPerformed(ActionEvent event) {
        	
            Chaos.exportImage();
        
        }
    
	}
	
	public static class Exit extends AbstractAction {
		
		private static final long serialVersionUID = 1L;
		
        public Exit(String name, String description, ImageIcon icon, int mnemonic) {
        	
            super(name, icon);
            putValue(SHORT_DESCRIPTION, description);
            putValue(MNEMONIC_KEY, mnemonic);
        
        }

        @Override
        public void actionPerformed(ActionEvent event) {
        	
            Chaos.exit();
        
        }
    
	}
	
	public static class About extends AbstractAction {
		
		private static final long serialVersionUID = 1L;
		
        public About(String name, String description, ImageIcon icon, int mnemonic) {
        	
            super(name, icon);
            putValue(SHORT_DESCRIPTION, description);
            putValue(MNEMONIC_KEY, mnemonic);
        
        }

        @Override
        public void actionPerformed(ActionEvent event) {
        	
            Chaos.showAboutDialog();
        
        }
		
	}
	
	public static class ApplySettings extends AbstractAction {
		
		private static final long serialVersionUID = 1L;
		
        public ApplySettings(String name, String description, ImageIcon icon, int mnemonic) {
        	
            super(name, icon);
            putValue(SHORT_DESCRIPTION, description);
            putValue(MNEMONIC_KEY, mnemonic);
        
        }

        @Override
        public void actionPerformed(ActionEvent event) {
        	
            Chaos.applySettings();
        
        }
		
	}
	
	public static class ApplyAndCloseSettings extends AbstractAction {
		
		private static final long serialVersionUID = 1L;
		
        public ApplyAndCloseSettings(String name, String description, ImageIcon icon, int mnemonic) {
        	
            super(name, icon);
            putValue(SHORT_DESCRIPTION, description);
            putValue(MNEMONIC_KEY, mnemonic);
        
        }

        @Override
        public void actionPerformed(ActionEvent event) {
        	
            Chaos.applySettings();
            Chaos.toggleSettingsVisibility();
        
        }
		
	}
	
	public static class CancelAndCloseSettings extends AbstractAction {
		
		private static final long serialVersionUID = 1L;
		
        public CancelAndCloseSettings(String name, String description, ImageIcon icon, int mnemonic) {
        	
            super(name, icon);
            putValue(SHORT_DESCRIPTION, description);
            putValue(MNEMONIC_KEY, mnemonic);
        
        }

        @Override
        public void actionPerformed(ActionEvent event) {
        	
            Chaos.toggleSettingsVisibility();
        
        }
		
	}
	
}