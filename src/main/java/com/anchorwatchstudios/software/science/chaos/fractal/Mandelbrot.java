package com.anchorwatchstudios.software.science.chaos.fractal;

import java.awt.Color;
import java.awt.image.BufferedImage;

import com.anchorwatchstudios.barracuda.math.Calc;
import com.anchorwatchstudios.barracuda.math.ComplexNumber;

public class Mandelbrot extends EscapeTimeFractal {
	
	public static final ComplexNumber DEFAULT_Z = new ComplexNumber(0, 0);
	public static final double DEFAULT_THRESHOLD = 2.0;
	public static final double DEFAULT_MINX = -2.125;
	public static final double DEFAULT_MAXX = 0.875;
	public static final double DEFAULT_MINY = -1.5;
	public static final double DEFAULT_MAXY = 1.5;

	private ComplexNumber Z;

	public Mandelbrot() {
		
		Z = DEFAULT_Z;
		setType(Type.MANDELBROT);
		setThreshold(DEFAULT_THRESHOLD);
		setMinX(DEFAULT_MINX);
		setMaxX(DEFAULT_MAXX);
		setMinY(DEFAULT_MINY);
		setMaxY(DEFAULT_MAXY);
		setAntiAlias(DEFAULT_ANTIALIAS);
		
	}
	
	@Override
	public BufferedImage render() {
		
		BufferedImage image = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
		Color[] color = getColor();
		
		for(int i=0;i<image.getWidth();i++) {
			
			for(int j=0;j<image.getHeight();j++) {
				
				double a = (double) i * (getMaxX() - getMinX()) / (double) image.getWidth() + getMinX();		
				double b = (double) j * (getMaxY() - getMinY()) / (double) image.getHeight() + getMinY();
				ComplexNumber c = new ComplexNumber(a,b);
				
				double value = isInSet(c);
				Color pixelColor;
				
				if(value < getIterations()) {
					
					int red1 = (int) Math.floor(((double) value / (double) getIterations()) * (double) color[1].getRed());
					int green1 = (int) Math.floor(((double) value / (double) getIterations()) * (double) color[1].getGreen());
					int blue1 = (int) Math.floor(((double) value / (double) getIterations()) * (double) color[1].getBlue());
					Color color1 = new Color(color[1].getAlpha() <<24 | red1 <<16 | green1 <<8 | blue1);

					if(getAntiAlias()) {

						int red2 = (int) Math.floor(((double) (value + 1) / (double) getIterations()) * (double) color[1].getRed());
						int green2 = (int) Math.floor(((double) (value + 1) / (double) getIterations()) * (double) color[1].getGreen());
						int blue2 = (int) Math.floor(((double) (value + 1) / (double) getIterations()) * (double) color[1].getBlue());
						Color color2 = new Color(color[1].getAlpha() <<24 | red2 <<16 | green2 <<8 | blue2);
						
						int red = Calc.interpolate(color2.getRed(), color1.getRed(), value / getIterations());
						int green = Calc.interpolate(color2.getGreen(), color1.getGreen(), value / getIterations());
						int blue = Calc.interpolate(color2.getBlue(), color1.getBlue(), value / getIterations());

						if(red >= 0 && green >= 0 && blue >= 0) {
							
							pixelColor = new Color(red, green, blue);
							
						}
						else {
							
							pixelColor = Color.BLACK;
							
						}
						
					}
					else {
						
						pixelColor = color1;
						
					}
					
				}
				else {
					
					pixelColor = color[0];
					
				}

				image.setRGB(i, j, pixelColor.getRGB());
				
			}
			
		}
		
		return image;
		
	}
		
	public ComplexNumber getZ() {
		
		return Z;
		
	}
	
	public void setZ(ComplexNumber Z) {
		
		this.Z = Z;
		
	}

	private double isInSet(ComplexNumber z) {
		
		double count = 0;
		ComplexNumber ZCopy = Z;
		
		for(int i=0;i<getIterations();i++) {
			
			ZCopy = ZCopy.square().add(z);
			
			if(ZCopy.abs() < Math.pow(getThreshold(), 2.0)) {
				
				count++;
				
			}
			else {
				
				if(getAntiAlias()) {

					count = getPreciseIterations(ZCopy, count);
					
				}
				
				break;
				
			}
			
		}
		
		return count;
		
	}
	
	private double getPreciseIterations(ComplexNumber c, double iterations) {

	    double log_zn = Math.log(c.abs()) / 2;
	    double nu = Math.log( log_zn / Math.log(2) ) / Math.log(2);
	    return iterations + 1 - nu;
		
	}
	
}