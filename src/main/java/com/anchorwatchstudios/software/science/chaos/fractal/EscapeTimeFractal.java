package com.anchorwatchstudios.software.science.chaos.fractal;

import java.awt.Color;
import java.awt.image.BufferedImage;

public abstract class EscapeTimeFractal {

	public static final Type DEFAULT_TYPE = Type.JULIA;
	public static final int DEFAULT_ITERATIONS = 100;
	public static final double DEFAULT_THRESHOLD = 2.0;
	public static final int DEFAULT_WIDTH = 1024;
	public static final int DEFAULT_HEIGHT = 1024;
	public static final double DEFAULT_MINX = -1.5;
	public static final double DEFAULT_MAXX = 1.5;
	public static final double DEFAULT_MINY = -1.5;
	public static final double DEFAULT_MAXY = 1.5;
	public static final double DEFAULT_ZOOM_INCREMENT = 0.1;
	public static final double DEFAULT_MOVE_INCREMENT = 0.1;
	public static final Color[] DEFAULT_COLOR = {Color.BLACK, Color.WHITE};
	public static final boolean DEFAULT_ANTIALIAS = false;
	private Type type = DEFAULT_TYPE;
	private int iterations = DEFAULT_ITERATIONS;
	private double threshold = DEFAULT_THRESHOLD;
	private int width = DEFAULT_WIDTH;
	private int height = DEFAULT_HEIGHT;
	private double minX = DEFAULT_MINX;
	private double maxX = DEFAULT_MAXX;
	private double minY = DEFAULT_MINY;
	private double maxY = DEFAULT_MAXY;
	private double zoomIncrement = DEFAULT_ZOOM_INCREMENT;
	private double moveIncrement = DEFAULT_MOVE_INCREMENT;
	private Color[] color = DEFAULT_COLOR;
	private boolean antiAlias;
	
	public abstract BufferedImage render();
	
	public void zoomIn() {
		
		double xInterval = Math.abs(maxX - minX);
		double yInterval = Math.abs(maxY - minY);
		minX += xInterval * zoomIncrement / 2;
		maxX -= xInterval * zoomIncrement / 2;
		minY += yInterval * zoomIncrement / 2;
		maxY -= yInterval * zoomIncrement / 2;
		
	}
	
	public void zoomOut() {

		double xInterval = Math.abs(maxX - minX);
		double yInterval = Math.abs(maxY - minY);
		minX -= xInterval * zoomIncrement / 2;
		maxX += xInterval * zoomIncrement / 2;
		minY -= yInterval * zoomIncrement / 2;
		maxY += yInterval * zoomIncrement / 2;
		
	}

	public void moveUp() {
		
		double interval = Math.abs(maxY - minY);
		minY -= interval * moveIncrement;
		maxY -= interval * moveIncrement;
		
	}
	
	public void moveDown() {
		
		double interval = Math.abs(maxY - minY);
		minY += interval * moveIncrement;
		maxY += interval * moveIncrement;
		
	}
	
	public void moveLeft() {
		
		double interval = Math.abs(maxX - minX);
		minX -= interval * moveIncrement;
		maxX -= interval * moveIncrement;
		
	}
	
	public void moveRight() {
		
		double interval = Math.abs(maxX - minX);
		minX += interval * moveIncrement;
		maxX += interval * moveIncrement;
		
	}
	
	public Type getType() {
		
		return type;
		
	}
	
	public void setType(Type type) {
		
		this.type = type;
		
	}
	
	public int getIterations() {
		
		return iterations;
		
	}
	
	public void setIterations(int iterations) {
		
		this.iterations = iterations;
		
	}
	
	public double getThreshold() {
		
		return threshold;
		
	}
	
	public void setThreshold(double threshold) {
		
		this.threshold = threshold;
		
	}

	public int getWidth() {
		
		return width;
		
	}
	
	public void setWidth(int width) {
		
		this.width = width;
		
	}
	
	public int getHeight() {
		
		return height;
		
	}
	
	public void setHeight(int height) {
		
		this.height = height;
		
	}
	
	public double getMinX() {
		
		return minX;
		
	}
	
	public void setMinX(double minX) {
		
		this.minX = minX;
		
	}
	
	public double getMaxX() {
		
		return maxX;
		
	}
	
	public void setMaxX(double maxX) {
		
		this.maxX = maxX;
		
	}
	
	public double getMinY() {
		
		return minY;
		
	}
	
	public void setMinY(double minY) {
		
		this.minY = minY;
		
	}
	
	public double getMaxY() {
		
		return maxY;
		
	}
	
	public void setMaxY(double maxY) {
		
		this.maxY = maxY;
		
	}
		
	public double getZoomIncrement() {
		
		return zoomIncrement;
		
	}
	
	public void setZoomIncrement(double zoomIncrement) {
		
		this.zoomIncrement = zoomIncrement;
		
	}
	
	public double getMoveIncrement() {
		
		return moveIncrement;
		
	}
	
	public void setMoveIncrement(double moveIncrement) {
		
		this.moveIncrement = zoomIncrement;
		
	}
	
	public Color[] getColor() {
		
		return color;
		
	}
	
	public void setColor(Color[] color) {
		
		this.color = color;
		
	}
			
	public boolean getAntiAlias() {
		
		return antiAlias;
		
	}
	
	public void setAntiAlias(boolean interpolate) {
		
		this.antiAlias = interpolate;
		
	}
	
}