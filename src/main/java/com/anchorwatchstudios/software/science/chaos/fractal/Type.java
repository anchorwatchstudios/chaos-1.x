package com.anchorwatchstudios.software.science.chaos.fractal;

public enum Type {
	
	JULIA,
	MANDELBROT,
	NEWTON
	
}