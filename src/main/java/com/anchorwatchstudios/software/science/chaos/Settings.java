package com.anchorwatchstudios.software.science.chaos;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.NumberFormat;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import com.anchorwatchstudios.barracuda.math.ComplexNumber;
import com.anchorwatchstudios.software.science.chaos.fractal.EscapeTimeFractal;
import com.anchorwatchstudios.software.science.chaos.fractal.Julia;
import com.anchorwatchstudios.software.science.chaos.fractal.Mandelbrot;
import com.anchorwatchstudios.software.science.chaos.fractal.Newton;

public class Settings implements PropertyChangeListener {

	private static final int WIDTH = 500;
	private static final int HEIGHT = 330;
	private JDialog window;
	private JButton applyButton;
	private JTabbedPane tabs;
	private JFormattedTextField minXField;
	private JFormattedTextField maxXField;
	private JFormattedTextField minYField;
	private JFormattedTextField maxYField;
	private JFormattedTextField zoomIncrement;
	private JFormattedTextField moveIncrement;
	private JFormattedTextField width;
	private JFormattedTextField height;
	private JButton color1;
	private JButton color2;
	private JCheckBox antiAlias;
	private NumberFormat integerFormat;
	private NumberFormat doubleFormat;

	public Settings(EscapeTimeFractal fractal) {

		integerFormat = NumberFormat.getNumberInstance();
		integerFormat.setGroupingUsed(false);
		doubleFormat = NumberFormat.getNumberInstance();
		doubleFormat.setMinimumFractionDigits(1);
		doubleFormat.setGroupingUsed(false);
		initUI();
		setAll(fractal);
		tabs.remove(2);

	}

	private void initUI() {

		window = new JDialog();
		window.setTitle("Settings");
		window.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		window.setSize(WIDTH, HEIGHT);
		window.setLocationRelativeTo(null);
		window.setIconImage(Chaos.ICON.getImage());
		window.setModal(true);
		window.addWindowListener(new WindowListener() {

			@Override
			public void windowActivated(WindowEvent arg0) {

			}

			@Override
			public void windowClosed(WindowEvent arg0) {

			}

			@Override
			public void windowClosing(WindowEvent arg0) {

				toggleVisibility(null);

			}

			@Override
			public void windowDeactivated(WindowEvent arg0) {

			}

			@Override
			public void windowDeiconified(WindowEvent arg0) {

			}

			@Override
			public void windowIconified(WindowEvent arg0) {

			}

			@Override
			public void windowOpened(WindowEvent arg0) {

			}

		});

		tabs = new JTabbedPane();
		tabs.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 5));
		JPanel graphPanel = new JPanel(new GridBagLayout());
		graphPanel.setBorder(BorderFactory.createEmptyBorder(25, 25, 25, 25));
		JPanel minMaxPanel = new JPanel(new GridBagLayout());
		minMaxPanel.setBorder(BorderFactory.createTitledBorder(Text.SCALE));
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 0.0;
		c.weighty = 1.0;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(5, 5, 5, 5);
		minMaxPanel.add(new JLabel(Text.MINX), c);
		c.gridy = 1;
		minMaxPanel.add(new JLabel(Text.MAXX), c);
		c.gridy = 2;
		minMaxPanel.add(new JLabel(Text.MAXY), c);
		c.gridy = 3;
		minMaxPanel.add(new JLabel(Text.MINY), c);
		c.gridx = 1;
		c.gridy = 0;
		c.weightx = 1.0;
		minXField = new JFormattedTextField(doubleFormat);
		minXField.addPropertyChangeListener("value", this);
		minMaxPanel.add(minXField, c);
		c.gridy = 1;
		maxXField = new JFormattedTextField(doubleFormat);
		maxXField.addPropertyChangeListener("value", this);
		minMaxPanel.add(maxXField, c);
		c.gridy = 2;
		minYField = new JFormattedTextField(doubleFormat);
		minYField.addPropertyChangeListener("value", this);
		minMaxPanel.add(minYField, c);
		c.gridy = 3;
		maxYField = new JFormattedTextField(doubleFormat);
		maxYField.addPropertyChangeListener("value", this);
		minMaxPanel.add(maxYField, c);
		c.gridx = 0;
		c.gridy = 0;
		graphPanel.add(minMaxPanel, c);
		JPanel incrementPanel = new JPanel(new GridBagLayout());
		incrementPanel.setBorder(BorderFactory.createEmptyBorder(25, 25, 25, 25));
		c.weightx = 0.0;
		incrementPanel.add(new JLabel(Text.ZOOM_INCREMENT), c);
		c.gridy = 1;
		incrementPanel.add(new JLabel(Text.MOVE_INCREMENT), c);
		c.gridx = 1;
		c.gridy = 0;
		c.weightx = 1.0;
		zoomIncrement = new JFormattedTextField(doubleFormat);
		zoomIncrement.addPropertyChangeListener("value", this);
		incrementPanel.add(zoomIncrement, c);
		c.gridy = 1;
		moveIncrement = new JFormattedTextField(doubleFormat);
		moveIncrement.addPropertyChangeListener("value", this);
		incrementPanel.add(moveIncrement, c);
		c.gridy = 0;
		graphPanel.add(incrementPanel, c);
		tabs.add(graphPanel, "Graph");
		JPanel imagePanel = new JPanel(new GridBagLayout());
		JPanel dimensionsPanel = new JPanel(new GridBagLayout());
		dimensionsPanel.setBorder(BorderFactory.createTitledBorder(Text.DIMENSIONS));
		c.gridx = 0;
		c.weightx = 0.0;
		dimensionsPanel.add(new JLabel(Text.WIDTH), c);
		c.gridx = 2;
		dimensionsPanel.add(new JLabel(Text.HEIGHT), c);
		width = new JFormattedTextField(integerFormat);
		width.addPropertyChangeListener("value", this);
		c.gridx = 1;
		c.weightx = 1.0;
		dimensionsPanel.add(width, c);
		height = new JFormattedTextField(integerFormat);
		height.addPropertyChangeListener("value", this);
		c.gridx = 3;
		dimensionsPanel.add(height, c);
		c.gridx = 0;
		c.weighty = 0.0;
		imagePanel.add(dimensionsPanel, c);
		JPanel colorPanel = new JPanel(new GridBagLayout());
		colorPanel.setBorder(BorderFactory.createTitledBorder(Text.COLOR));
		c.weightx = 0.0;
		c.weighty = 1.0;
		colorPanel.add(new JLabel(Text.COLOR1), c);
		c.gridx = 2;
		colorPanel.add(new JLabel(Text.COLOR2), c);
		color1 = new JButton(Text.BLANK_MESSAGE);
		color1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				Color color = JColorChooser.showDialog(null, Text.COLOR, color1.getBackground());

				if(color != null) {

					color1.setBackground(color);
					applyButton.setEnabled(true);

				}

			}

		});
		color1.setFocusable(false);
		color1.setBorder(BorderFactory.createTitledBorder(""));
		c.gridx = 1;
		c.weightx = 1.0;
		colorPanel.add(color1, c);
		color2 = new JButton(Text.BLANK_MESSAGE);
		color2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				Color color = JColorChooser.showDialog(null, Text.COLOR, color2.getBackground());

				if(color != null) {

					color2.setBackground(color);
					applyButton.setEnabled(true);

				}				
			}

		});
		color2.setFocusable(false);
		color2.setBorder(BorderFactory.createTitledBorder(""));
		c.gridx = 3;
		colorPanel.add(color2, c);
		c.gridx = 0;
		c.gridy = 1;
		c.weighty = 0.0;
		imagePanel.add(colorPanel, c);
		c.gridy = 2;
		c.weightx = 1.0;
		antiAlias = new JCheckBox(Text.ANTIALIAS);
		antiAlias.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				propertyChange(null);
				
			}
			
		});
		imagePanel.add(antiAlias, c);
		tabs.add(imagePanel, "Image");
		window.add(tabs, BorderLayout.CENTER);

		JPanel buttonPanel = new JPanel();
		GridLayout g = new GridLayout(1, 6);
		g.setHgap(5);
		buttonPanel.setLayout(g);
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		buttonPanel.add(Box.createGlue());
		buttonPanel.add(Box.createGlue());
		buttonPanel.add(Box.createGlue());
		buttonPanel.add(new JButton(new Action.ApplyAndCloseSettings(Text.BUTTON_OK, "", null, KeyEvent.VK_O)));
		applyButton = new JButton(new Action.ApplySettings(Text.BUTTON_APPLY, "", null, KeyEvent.VK_A));
		applyButton.setEnabled(false);
		buttonPanel.add(applyButton);
		buttonPanel.add(new JButton(new Action.CancelAndCloseSettings(Text.BUTTON_CANCEL, "", null, KeyEvent.VK_C)));
		window.add(buttonPanel, BorderLayout.SOUTH);

	}

	public EscapeTimeFractal apply(EscapeTimeFractal fractal) {

		fractal.setMinX(Double.parseDouble(minXField.getText()));
		fractal.setMaxX(Double.parseDouble(maxXField.getText()));
		fractal.setMinY(Double.parseDouble(minYField.getText()));
		fractal.setMaxY(Double.parseDouble(maxYField.getText()));
		fractal.setZoomIncrement(Double.parseDouble(zoomIncrement.getText()) / 100);
		fractal.setMoveIncrement(Double.parseDouble(moveIncrement.getText()) / 100);
		fractal.setWidth(Integer.parseInt(width.getText()));
		fractal.setHeight(Integer.parseInt(height.getText()));
		fractal.setColor(new Color[] { color1.getBackground(), color2.getBackground() });
		fractal.setAntiAlias(antiAlias.isSelected());

		switch(fractal.getType()) {

		case JULIA:

			JuliaPanel juliaPanel = (JuliaPanel) tabs.getComponentAt(2);
			Julia julia = (Julia) fractal;
			julia.setIterations(juliaPanel.getIterations());
			julia.setThreshold(juliaPanel.getThreshold());
			julia.setC(new ComplexNumber(juliaPanel.getCa(), juliaPanel.getCb()));

			break;

		case MANDELBROT:

			MandelbrotPanel mandelbrotPanel = (MandelbrotPanel) tabs.getComponentAt(2);
			Mandelbrot mandelbrot = (Mandelbrot) fractal;
			mandelbrot.setIterations(mandelbrotPanel.getIterations());
			mandelbrot.setThreshold(mandelbrotPanel.getThreshold());
			mandelbrot.setZ(new ComplexNumber(mandelbrotPanel.getZa(), mandelbrotPanel.getZb()));
			break;

		case NEWTON:

			NewtonPanel newtonPanel = (NewtonPanel) tabs.getComponentAt(2);
			Newton newton = (Newton) fractal;
			newton.setIterations(newtonPanel.getIterations());
			newton.setThreshold(newtonPanel.getThreshold());
			newton.setN(new ComplexNumber(newtonPanel.getNa(), newtonPanel.getNb()));
			newton.setAlpha(new ComplexNumber(newtonPanel.getAa(), newtonPanel.getAb()));
			break;

		}

		applyButton.setEnabled(false);
		return fractal;

	}

	public void toggleVisibility(EscapeTimeFractal fractal) {

		if(window.isVisible()) {

			window.setVisible(false);
			tabs.remove(2);

		}
		else {

			setAll(fractal);
			window.setVisible(true);

		}

	}

	private void setAll(EscapeTimeFractal fractal) {

		minXField.setValue(fractal.getMinX());
		maxXField.setValue(fractal.getMaxX());
		minYField.setValue(fractal.getMinY());
		maxYField.setValue(fractal.getMaxY());
		zoomIncrement.setValue(fractal.getZoomIncrement() * 100);
		moveIncrement.setValue(fractal.getMoveIncrement() * 100);
		width.setValue(fractal.getWidth());
		height.setValue(fractal.getHeight());
		color1.setBackground(fractal.getColor()[0]);
		color2.setBackground(fractal.getColor()[1]);
		antiAlias.setSelected(fractal.getAntiAlias());

		switch(fractal.getType()) {

		case JULIA:

			tabs.add(new JuliaPanel(fractal), Text.FRACTAL);
			break;

		case MANDELBROT:

			tabs.add(new MandelbrotPanel(fractal), Text.FRACTAL);
			break;

		case NEWTON:

			tabs.add(new NewtonPanel(fractal), Text.FRACTAL);

		}

		applyButton.setEnabled(false);
		
	}

	private class JuliaPanel extends JPanel {

		private static final long serialVersionUID = 1L;
		private JFormattedTextField iterations;
		private JFormattedTextField threshold;
		private JFormattedTextField Ca;
		private JFormattedTextField Cb;

		private JuliaPanel(EscapeTimeFractal fractal) {

			setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 0;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.insets = new Insets(5, 5, 5, 5);
			add(new JLabel(fractal.getType().toString() + " " + Text.JULIA_EQUATION), c);
			iterations = new JFormattedTextField(integerFormat);
			iterations.setValue(fractal.getIterations());
			iterations.addPropertyChangeListener("value", Settings.this);
			c.gridy = 1;
			c.weightx = 0.0;
			add(new JLabel(Text.ITERATIONS), c);
			c.gridx = 1;
			c.weightx = 1.0;
			add(iterations, c);
			threshold = new JFormattedTextField(doubleFormat);
			threshold.setValue(fractal.getThreshold());
			threshold.addPropertyChangeListener("value", Settings.this);
			c.gridx = 0;
			c.gridy = 2;
			c.weightx = 0.0;
			add(new JLabel(Text.THRESHOLD), c);
			c.gridx = 1;
			c.weightx = 1.0;
			add(threshold, c);
			Ca = new JFormattedTextField(doubleFormat);
			Ca.setValue(((Julia) fractal).getC().getReal());
			Ca.addPropertyChangeListener("value", Settings.this);
			Cb = new JFormattedTextField(doubleFormat);
			Cb.setValue(((Julia) fractal).getC().getImaginary());
			Cb.addPropertyChangeListener("value", Settings.this);
			c.gridx = 0;
			c.gridy = 3;
			c.weightx = 0.0;
			add(new JLabel("C = ("), c);
			c.weightx = 1.0;
			c.gridx = 1;
			add(Ca, c);
			c.gridx = 2;
			c.weightx = 0.0;
			add(new JLabel(" + "), c);
			c.gridx = 3;
			c.weightx = 1.0;
			add(Cb, c);
			c.gridx = 4;
			c.weightx = 0.0;
			add(new JLabel("i)"), c);

		}

		private int getIterations() {

			return Integer.parseInt(iterations.getText());

		}

		private double getThreshold() {

			return Double.parseDouble(threshold.getText());

		}

		private double getCa() {

			return Double.parseDouble(Ca.getText());

		}

		private double getCb() {

			return Double.parseDouble(Cb.getText());

		}

	}

	private class MandelbrotPanel extends JPanel {

		private static final long serialVersionUID = 1L;
		private JFormattedTextField iterations;
		private JFormattedTextField threshold;
		private JFormattedTextField Za;
		private JFormattedTextField Zb;

		private MandelbrotPanel(EscapeTimeFractal fractal) {

			setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 0;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.insets = new Insets(5, 5, 5, 5);
			add(new JLabel(fractal.getType().toString() + " " + Text.MANDELBROT_EQUATION), c);
			iterations = new JFormattedTextField(integerFormat);
			iterations.setValue(fractal.getIterations());
			iterations.addPropertyChangeListener("value", Settings.this);
			c.gridy = 1;
			c.weightx = 0.0;
			add(new JLabel(Text.ITERATIONS), c);
			c.gridx = 1;
			c.weightx = 1.0;
			add(iterations, c);
			threshold = new JFormattedTextField(doubleFormat);
			threshold.setValue(fractal.getThreshold());
			threshold.addPropertyChangeListener("value", Settings.this);
			c.gridx = 0;
			c.gridy = 2;
			c.weightx = 0.0;
			add(new JLabel(Text.THRESHOLD), c);
			c.gridx = 1;
			c.weightx = 1.0;
			add(threshold, c);
			Za = new JFormattedTextField(doubleFormat);
			Za.setValue(((Mandelbrot) fractal).getZ().getReal());
			Za.addPropertyChangeListener("value", Settings.this);
			Zb = new JFormattedTextField(doubleFormat);
			Zb.setValue(((Mandelbrot) fractal).getZ().getImaginary());
			Zb.addPropertyChangeListener("value", Settings.this);
			c.gridx = 0;
			c.gridy = 3;
			c.weightx = 0.0;
			add(new JLabel("Z = ("), c);
			c.weightx = 1.0;
			c.gridx = 1;
			add(Za, c);
			c.gridx = 2;
			c.weightx = 0.0;
			add(new JLabel(" + "), c);
			c.gridx = 3;
			c.weightx = 1.0;
			add(Zb, c);
			c.gridx = 4;
			c.weightx = 0.0;
			add(new JLabel("i)"), c);

		}

		private int getIterations() {

			return Integer.parseInt(iterations.getText());

		}

		private double getThreshold() {

			return Double.parseDouble(threshold.getText());

		}

		private double getZa() {

			return Double.parseDouble(Za.getText());

		}

		private double getZb() {

			return Double.parseDouble(Zb.getText());

		}

	}

	private class NewtonPanel extends JPanel {

		private static final long serialVersionUID = 1L;
		private JFormattedTextField iterations;
		private JFormattedTextField threshold;
		private JFormattedTextField Na;
		private JFormattedTextField Nb;
		private JFormattedTextField Aa;
		private JFormattedTextField Ab;

		private NewtonPanel(EscapeTimeFractal fractal) {

			setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 0;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.insets = new Insets(5, 5, 5, 5);
			add(new JLabel(fractal.getType().toString() + " " + Text.NEWTON_EQUATION), c);
			iterations = new JFormattedTextField(integerFormat);
			iterations.setValue(fractal.getIterations());
			iterations.addPropertyChangeListener("value", Settings.this);
			c.gridy = 1;
			c.weightx = 0.0;
			add(new JLabel(Text.ITERATIONS), c);
			c.gridx = 1;
			c.weightx = 1.0;
			add(iterations, c);
			threshold = new JFormattedTextField(doubleFormat);
			threshold.setValue(fractal.getThreshold());
			threshold.addPropertyChangeListener("value", Settings.this);
			c.gridx = 0;
			c.gridy = 2;
			c.weightx = 0.0;
			add(new JLabel(Text.THRESHOLD), c);
			c.gridx = 1;
			c.weightx = 1.0;
			add(threshold, c);
			Na = new JFormattedTextField(doubleFormat);
			Na.setValue(((Newton) fractal).getN().getReal());
			Na.addPropertyChangeListener("value", Settings.this);
			Nb = new JFormattedTextField(doubleFormat);
			Nb.setValue(((Newton) fractal).getN().getImaginary());
			Nb.addPropertyChangeListener("value", Settings.this);
			c.gridx = 0;
			c.gridy = 3;
			c.weightx = 0.0;
			add(new JLabel("N = ("), c);
			c.weightx = 1.0;
			c.gridx = 1;
			add(Na, c);
			c.gridx = 2;
			c.weightx = 0.0;
			add(new JLabel(" + "), c);
			c.gridx = 3;
			c.weightx = 1.0;
			add(Nb, c);
			c.gridx = 4;
			c.weightx = 0.0;
			add(new JLabel("i)"), c);
			Aa = new JFormattedTextField(doubleFormat);
			Aa.setValue(((Newton) fractal).getAlpha().getReal());
			Aa.addPropertyChangeListener("value", Settings.this);
			Ab = new JFormattedTextField(doubleFormat);
			Ab.setValue(((Newton) fractal).getAlpha().getImaginary());
			Ab.addPropertyChangeListener("value", Settings.this);
			c.gridx = 0;
			c.gridy = 4;
			c.weightx = 0.0;
			add(new JLabel("A = ("), c);
			c.weightx = 1.0;
			c.gridx = 1;
			add(Aa, c);
			c.gridx = 2;
			c.weightx = 0.0;
			add(new JLabel(" + "), c);
			c.gridx = 3;
			c.weightx = 1.0;
			add(Ab, c);
			c.gridx = 4;
			c.weightx = 0.0;
			add(new JLabel("i)"), c);

		}

		private int getIterations() {

			return Integer.parseInt(iterations.getText());

		}

		private double getThreshold() {

			return Double.parseDouble(threshold.getText());

		}

		private double getNa() {

			return Double.parseDouble(Na.getText());

		}

		private double getNb() {

			return Double.parseDouble(Nb.getText());

		}

		private double getAa() {

			return Double.parseDouble(Aa.getText());

		}

		private double getAb() {

			return Double.parseDouble(Ab.getText());

		}

	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {

		applyButton.setEnabled(true);

	}

}