package com.anchorwatchstudios.software.science.chaos;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;
import javax.swing.filechooser.FileFilter;

import com.anchorwatchstudios.resource.api.seahorse.Icon;
import com.anchorwatchstudios.resource.api.seahorse.IconFactory;
import com.anchorwatchstudios.software.science.chaos.fractal.Document;
import com.anchorwatchstudios.software.science.chaos.fractal.EscapeTimeFractal;
import com.anchorwatchstudios.software.science.chaos.fractal.Julia;
import com.anchorwatchstudios.software.science.chaos.fractal.Mandelbrot;
import com.anchorwatchstudios.software.science.chaos.fractal.Newton;
import com.anchorwatchstudios.software.science.chaos.fractal.Parser;
import com.anchorwatchstudios.software.science.chaos.fractal.Type;

public class Chaos {

	public static final ImageIcon ICON = new ImageIcon(Chaos.class.getClass().getResource("/com/anchorwatchstudios/software/science/chaos/res/icon_64x64.png"));
	public static final int VERSION_MAJOR = 0;
	public static final int VERSION_MINOR = 1;
	public static final int VERSION_PATCH = 0;
	private static final int WIDTH = 901;
	private static final int HEIGHT = 1024;
	private static JFrame window;
	private static ImagePanel imagePanel;
	private static JLabel statusLabel;
	private static Settings settings;
	private static JFileChooser fileChooser;
	private static JFileChooser exportFileChooser;
	private static Document document;
	private static EscapeTimeFractal fractal;

	public static void main(String[] args) {

		new Chaos();

	}

	private Chaos() {
	
		initDocument();
		initUI();

	}

	private static void initUI() {

		window = new JFrame();
		window.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		window.addWindowListener(new WindowListener() {

			@Override
			public void windowActivated(WindowEvent arg0) {
				
			}

			@Override
			public void windowClosed(WindowEvent arg0) {

			}

			@Override
			public void windowClosing(WindowEvent arg0) {

				exit();
				
			}

			@Override
			public void windowDeactivated(WindowEvent arg0) {
				
			}

			@Override
			public void windowDeiconified(WindowEvent arg0) {
				
			}

			@Override
			public void windowIconified(WindowEvent arg0) {
				
			}

			@Override
			public void windowOpened(WindowEvent arg0) {
				
			}
			
		});
		window.setSize(WIDTH, HEIGHT);
		window.setLocationRelativeTo(null);
		window.setIconImage(ICON.getImage());
		updateWindowTitle();

		ImageIcon newIcon = IconFactory.createIcon(Icon.NEW, new Dimension(32, 32));
		ImageIcon openIcon = IconFactory.createIcon(Icon.OPEN, new Dimension(32, 32));
		ImageIcon saveIcon = IconFactory.createIcon(Icon.SAVE, new Dimension(32, 32));
		ImageIcon saveAsIcon = IconFactory.createIcon(Icon.SAVEAS, new Dimension(32, 32));
		ImageIcon zoomInIcon = IconFactory.createIcon(Icon.ZOOMIN, new Dimension(32, 32));
		ImageIcon zoomOutIcon = IconFactory.createIcon(Icon.ZOOMOUT, new Dimension(32, 32));
		ImageIcon upIcon = IconFactory.createIcon(Icon.ARROW_UP, new Dimension(32, 32));
		ImageIcon downIcon = IconFactory.createIcon(Icon.ARROW_DOWN, new Dimension(32, 32));
		ImageIcon leftIcon = IconFactory.createIcon(Icon.ARROW_LEFT, new Dimension(32, 32));
		ImageIcon rightIcon = IconFactory.createIcon(Icon.ARROW_RIGHT, new Dimension(32, 32));
		ImageIcon exportIcon = IconFactory.createIcon(Icon.EXPORT, new Dimension(32, 32));
		ImageIcon settingsIcon = IconFactory.createIcon(Icon.GEAR, new Dimension(32, 32));
		ImageIcon exitIcon = IconFactory.createIcon(Icon.EXIT, new Dimension(32, 32));
		ImageIcon aboutIcon = IconFactory.createIcon(Icon.INFO, new Dimension(32, 32));

		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu(Text.MENU_TEXT_FILE);
		JMenu newMenu = new JMenu(Text.MENU_TEXT_NEW);
		newMenu.setIcon(newIcon);
		newMenu.add(new JMenuItem(new Action.New(Text.MENU_TEXT_NEW_JULIA, "", newIcon, KeyEvent.VK_J, Type.JULIA)));
		newMenu.add(new JMenuItem(new Action.New(Text.MENU_TEXT_NEW_MANDELBROT, "", newIcon, KeyEvent.VK_M, Type.MANDELBROT)));
		newMenu.add(new JMenuItem(new Action.New(Text.MENU_TEXT_NEW_NEWTON, "", newIcon, KeyEvent.VK_N, Type.NEWTON)));
		fileMenu.add(newMenu);
		fileMenu.add(new JMenuItem(new Action.Open(Text.MENU_TEXT_OPEN, "", openIcon, KeyEvent.VK_O)));
		fileMenu.add(new JMenuItem(new Action.Save(Text.MENU_TEXT_SAVE, "", saveIcon, KeyEvent.VK_S)));
		fileMenu.add(new JMenuItem(new Action.SaveAs(Text.MENU_TEXT_SAVEAS, "", saveAsIcon, KeyEvent.VK_A)));
		fileMenu.addSeparator();
		fileMenu.add(new JMenuItem(new Action.ExportImage(Text.MENU_TEXT_EXPORT_IMAGE, "", exportIcon, KeyEvent.VK_I)));
		fileMenu.addSeparator();
		fileMenu.add(new JMenuItem(new Action.Exit(Text.MENU_TEXT_EXIT, "", exitIcon, KeyEvent.VK_E)));
		menuBar.add(fileMenu);
		JMenu editMenu = new JMenu(Text.MENU_TEXT_EDIT);
		editMenu.add(new JMenuItem(new Action.EditSettings(Text.MENU_TEXT_EDIT_SETTINGS, "", settingsIcon, KeyEvent.VK_G)));
		menuBar.add(editMenu);
		JMenu viewMenu = new JMenu(Text.MENU_TEXT_VIEW);
		viewMenu.add(new JMenuItem(new Action.ZoomIn(Text.MENU_TEXT_ZOOM_IN, "", zoomInIcon, KeyEvent.VK_Z)));
		viewMenu.add(new JMenuItem(new Action.ZoomOut(Text.MENU_TEXT_ZOOM_OUT, "", zoomOutIcon, KeyEvent.VK_Y)));
		viewMenu.add(new JMenuItem(new Action.MoveUp(Text.MENU_TEXT_MOVE_UP, "", upIcon, KeyEvent.VK_U)));
		viewMenu.add(new JMenuItem(new Action.MoveDown(Text.MENU_TEXT_MOVE_DOWN, "", downIcon, KeyEvent.VK_D)));
		viewMenu.add(new JMenuItem(new Action.MoveLeft(Text.MENU_TEXT_MOVE_LEFT, "", leftIcon, KeyEvent.VK_L)));
		viewMenu.add(new JMenuItem(new Action.MoveRight(Text.MENU_TEXT_MOVE_RIGHT, "", rightIcon, KeyEvent.VK_R)));
		menuBar.add(viewMenu);
		JMenu helpMenu = new JMenu(Text.MENU_TEXT_HELP);
		helpMenu.add(new JMenuItem(new Action.About(Text.MENU_TEXT_ABOUT, "", aboutIcon, KeyEvent.VK_A)));
		menuBar.add(helpMenu);
		window.setJMenuBar(menuBar);

		JToolBar toolBar = new JToolBar();
		toolBar.add(new JButton(new Action.Open("", Text.MENU_TEXT_OPEN, openIcon, KeyEvent.VK_O)));
		toolBar.add(new JButton(new Action.Save("", Text.MENU_TEXT_SAVE, saveIcon, KeyEvent.VK_S)));
		toolBar.add(new JButton(new Action.SaveAs("", Text.MENU_TEXT_SAVEAS, saveAsIcon, KeyEvent.VK_A)));
		toolBar.addSeparator();
		toolBar.add(new JButton(new Action.ExportImage("", Text.MENU_TEXT_EXPORT_IMAGE, exportIcon, KeyEvent.VK_I)));
		toolBar.addSeparator();
		toolBar.add(new JButton(new Action.ZoomIn("", Text.MENU_TEXT_ZOOM_IN, zoomInIcon, KeyEvent.VK_Z)));
		toolBar.add(new JButton(new Action.ZoomOut("", Text.MENU_TEXT_ZOOM_OUT, zoomOutIcon, KeyEvent.VK_Y)));
		toolBar.add(new JButton(new Action.MoveUp("", Text.MENU_TEXT_MOVE_UP, upIcon, KeyEvent.VK_U)));
		toolBar.add(new JButton(new Action.MoveDown("", Text.MENU_TEXT_MOVE_DOWN, downIcon, KeyEvent.VK_D)));
		toolBar.add(new JButton(new Action.MoveLeft("", Text.MENU_TEXT_MOVE_LEFT, leftIcon, KeyEvent.VK_L)));
		toolBar.add(new JButton(new Action.MoveRight("", Text.MENU_TEXT_MOVE_RIGHT, rightIcon, KeyEvent.VK_R)));
		toolBar.addSeparator();
		toolBar.add(new JButton(new Action.EditSettings("", Text.MENU_TEXT_EDIT_SETTINGS, settingsIcon, KeyEvent.VK_G)));
		window.add(toolBar, BorderLayout.NORTH);

		statusLabel = new JLabel();
		statusLabel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		window.add(statusLabel, BorderLayout.SOUTH);

		imagePanel = new ImagePanel();
		render();
		window.add(imagePanel, BorderLayout.CENTER);

		fileChooser = new JFileChooser() {

			private static final long serialVersionUID = 1L;

			@Override
			public void approveSelection() {

				File selectedFile = getSelectedFile();

				if(selectedFile.exists() && !selectedFile.equals(document.getFile()) && getDialogType() == SAVE_DIALOG) {

					switch(JOptionPane.showConfirmDialog(window, Text.OVERWRITE_MESSAGE, Text.OVERWRITE_MESSAGE_TITLE, JOptionPane.YES_NO_CANCEL_OPTION)) {

					case JOptionPane.YES_OPTION:

						super.approveSelection();
						break;

					default:

						cancelSelection();
						break;

					}

				}
				else {

					super.approveSelection();

				}

			}        

		};
		fileChooser.setAcceptAllFileFilterUsed(false);
		fileChooser.setFileFilter(new FileFilter() {

			@Override
			public boolean accept(File f) {

				return f.getName().toLowerCase().endsWith(Text.FILE_EXT_FRC) || f.isDirectory();

			}

			@Override
			public String getDescription() {

				return Text.FILE_DESC_FRC;

			}

		});
		fileChooser.setCurrentDirectory(new File(Text.DEFAULT_DIRECTORY));

		exportFileChooser = new JFileChooser() {

			private static final long serialVersionUID = 1L;

			@Override
			public void approveSelection() {

				File selectedFile = getSelectedFile();

				if(selectedFile.exists()) {

					switch(JOptionPane.showConfirmDialog(window, Text.OVERWRITE_MESSAGE, Text.OVERWRITE_MESSAGE_TITLE, JOptionPane.YES_NO_CANCEL_OPTION)) {

					case JOptionPane.YES_OPTION:

						super.approveSelection();
						break;

					default:

						cancelSelection();
						break;

					}

				}
				else {

					super.approveSelection();

				}

			}        

		};
		exportFileChooser.setAcceptAllFileFilterUsed(false);
		exportFileChooser.setFileFilter(new FileFilter() {

			@Override
			public boolean accept(File f) {

				return f.getName().toLowerCase().endsWith(Text.FILE_EXT_PNG) || f.isDirectory();

			}

			@Override
			public String getDescription() {

				return Text.FILE_DESC_PNG;

			}

		});
		exportFileChooser.setCurrentDirectory(new File(Text.DEFAULT_DIRECTORY));
		
		settings = new Settings(fractal);
		
		window.setVisible(true);

	}

	private static void initDocument() {
		
		document = new Document();
		fractal = new Julia();	
		
	}
	
	private synchronized static void render() {

		updateStatus(Text.RENDER_MESSAGE);

		new Thread(new Runnable() {

			@Override
			public void run() {

				imagePanel.setImage(fractal.render());
				imagePanel.repaint();
				updateStatus(Text.BLANK_MESSAGE);

			}

		}).start();

	}

	private static void updateStatus(String message) {

		statusLabel.setText(message);

	}

	private static void updateWindowTitle() {

		String title = document.getName();

		if(document.isEdited()) {

			title += Text.DOC_EDITED_SYMBOL;

		}

		window.setTitle(Text.TITLE + " " + VERSION_MAJOR + "." + VERSION_MINOR + "." + VERSION_PATCH + " - " + title);

	}

	public static void showAboutDialog() {

		JOptionPane.showMessageDialog(window, Text.ABOUT_MESSAGE, Text.ABOUT_MESSAGE_TITLE, JOptionPane.INFORMATION_MESSAGE, ICON);

	}
	
	public static void applySettings() {

			EscapeTimeFractal newFractal = settings.apply(fractal);
			document.setEdited(true);
			updateWindowTitle();
			
			if(newFractal != null) {
				
				fractal = newFractal;
				render();
				
			}

	}
	
	public static void zoomIn() {

		fractal.zoomIn();
		render();
		document.setEdited(true);
		updateWindowTitle();

	}

	public static void zoomOut() {

		fractal.zoomOut();
		render();
		document.setEdited(true);
		updateWindowTitle();

	}

	public static void moveUp() {

		fractal.moveUp();
		render();
		document.setEdited(true);
		updateWindowTitle();

	}

	public static void moveDown() {

		fractal.moveDown();
		render();
		document.setEdited(true);
		updateWindowTitle();

	}

	public static void moveLeft() {

		fractal.moveLeft();
		render();
		document.setEdited(true);
		updateWindowTitle();

	}

	public static void moveRight() {

		fractal.moveRight();
		render();
		document.setEdited(true);
		updateWindowTitle();

	}
	
	public static void toggleSettingsVisibility() {
		
		settings.toggleVisibility(fractal);

	}

	public static void newFractal(Type type) {

		if(checkIfModified()) {

			switch(type) {

			case JULIA:

				fractal = new Julia();
				break;

			case MANDELBROT:

				fractal = new Mandelbrot();
				break;

			case NEWTON:

				fractal = new Newton();
				break;

			}

			document = new Document();
			render();
			updateWindowTitle();

		}

	}

	public static void openFractal() {

		if(checkIfModified()) {

			if (fileChooser.showOpenDialog(window) == JFileChooser.APPROVE_OPTION) {

				updateStatus(Text.OPEN_MESSAGE);
				File selectedFile = fileChooser.getSelectedFile();

				try {

					fractal = Parser.fileToFractal(selectedFile);
					document = new Document(selectedFile);
					updateWindowTitle();
					render();

				} 
				catch(Exception e) {

					e.printStackTrace();
					JOptionPane.showMessageDialog(window, Text.OPEN_ERROR_MESSAGE, Text.ERROR_MESSAGE_TITLE, JOptionPane.ERROR_MESSAGE);

				}

				updateStatus(Text.BLANK_MESSAGE);

			}

		}

	}

	public static void saveFractal(boolean forceSaveDialog) {

		if(document.isNew() || forceSaveDialog == true) {

			if(fileChooser.showSaveDialog(window) == JFileChooser.APPROVE_OPTION) {

				File selectedFile = fileChooser.getSelectedFile();

				if(!selectedFile.getPath().endsWith(Text.FILE_EXT_FRC)) {

					selectedFile = new File(selectedFile.getPath() + Text.FILE_EXT_FRC);

				}

				try {

					updateStatus(Text.SAVE_MESSAGE);
					Parser.fractalToFile(fractal, selectedFile);
					document = new Document(selectedFile);
					updateWindowTitle();

				}
				catch(Exception e) {

					e.printStackTrace();
					JOptionPane.showMessageDialog(window, Text.SAVE_ERROR_MESSAGE, Text.ERROR_MESSAGE_TITLE, JOptionPane.ERROR_MESSAGE);

				}

			}

		}
		else {

			if(document.isEdited()) {

				try {

					updateStatus(Text.SAVE_MESSAGE);
					Parser.fractalToFile(fractal, document.getFile());
					document.setEdited(false);
					updateWindowTitle();

				} catch (Exception e) {

					e.printStackTrace();
					JOptionPane.showMessageDialog(window, Text.SAVE_ERROR_MESSAGE, Text.ERROR_MESSAGE_TITLE, JOptionPane.ERROR_MESSAGE);

				}

			}

		}

		updateStatus(Text.BLANK_MESSAGE);

	}

	public static void exportImage() {

		if(exportFileChooser.showDialog(window, Text.EXPORT_TITLE) == JFileChooser.APPROVE_OPTION) {

			File selectedFile = exportFileChooser.getSelectedFile();

			if(!selectedFile.getPath().endsWith(Text.FILE_EXT_PNG)) {

				selectedFile = new File(selectedFile.getPath() + Text.FILE_EXT_PNG);

			}

			try {

				updateStatus(Text.EXPORT_MESSAGE);
				ImageIO.write(imagePanel.getImage(), "png", selectedFile);

			}
			catch(Exception e) {

				e.printStackTrace();
				JOptionPane.showMessageDialog(window, Text.EXPORT_ERROR_MESSAGE, Text.ERROR_MESSAGE_TITLE, JOptionPane.ERROR_MESSAGE);

			}

			updateStatus(Text.BLANK_MESSAGE);

		}

	}

	private static boolean checkIfModified() {

		if(document.isEdited()) {

			int result = JOptionPane.showConfirmDialog(window, Text.UNSAVED_MESSAGE);
			
			if(result == JOptionPane.YES_OPTION) {

				saveFractal(false);
				return true;

			}
			else if(result == JOptionPane.NO_OPTION) {
				
				return true;
				
			}
			else {
				
				return false;
				
			}

		}
		else {
			
			return true;
			
		}

	}
	
	public static void exit() {

		if(checkIfModified()) {
			
			System.exit(0);
			
		}

	}

}