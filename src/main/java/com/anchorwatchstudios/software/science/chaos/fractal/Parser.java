package com.anchorwatchstudios.software.science.chaos.fractal;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.anchorwatchstudios.barracuda.math.ComplexNumber;

public class Parser {

	private Parser() {	

	}

	public static EscapeTimeFractal fileToFractal(File file) throws NumberFormatException, FileNotFoundException, IOException {

		BufferedReader in = new BufferedReader(new FileReader(file));
		EscapeTimeFractal fractal;
		Type type = Type.valueOf(in.readLine());

		switch(type) {

		case JULIA:

			fractal = new Julia();
			break;

		case MANDELBROT:

			fractal = new Mandelbrot();
			break;

		case NEWTON:

			fractal = new Newton();
			break;
			
		default:
			
			fractal = new Julia();
			break;

		}

		String[] loopVars = in.readLine().split(",");
		fractal.setIterations(Integer.valueOf(loopVars[0]));
		fractal.setThreshold(Double.valueOf(loopVars[1]));
		String[] dimension = in.readLine().split(",");
		fractal.setWidth(Integer.valueOf(dimension[0]));
		fractal.setHeight(Integer.valueOf(dimension[1]));
		String[] constraints = in.readLine().split(",");
		fractal.setMinX(Double.valueOf(constraints[0]));
		fractal.setMaxX(Double.valueOf(constraints[1]));
		fractal.setMinY(Double.valueOf(constraints[2]));
		fractal.setMaxY(Double.valueOf(constraints[3]));
		String[] increment = in.readLine().split(",");
		fractal.setZoomIncrement(Double.valueOf(increment[0]));
		fractal.setMoveIncrement(Double.valueOf(increment[1]));
		String[] colorString = in.readLine().split(",");
		Color[] color = new Color[colorString.length];
		
		for(int i=0;i<color.length;i++) {
			
			color[i] = new Color(Integer.valueOf(colorString[i]));
			
		}
		
		fractal.setColor(color);
		String[] var = in.readLine().split(",");

		switch(type) {

		case JULIA:

			((Julia) fractal).setC(new ComplexNumber(Double.valueOf(var[0]), Double.valueOf(var[1])));
			break;

		case MANDELBROT:

			((Mandelbrot) fractal).setZ(new ComplexNumber(Double.valueOf(var[0]), Double.valueOf(var[1])));
			break;

		case NEWTON:
			
			((Newton) fractal).setN(new ComplexNumber(Double.valueOf(var[0]), Double.valueOf(var[1])));
			((Newton) fractal).setAlpha(new ComplexNumber(Double.valueOf(var[2]), Double.valueOf(var[3])));
			break;
			
		default:

			((Julia) fractal).setC(new ComplexNumber(Double.valueOf(var[0]), Double.valueOf(var[1])));
			break;

		}

		in.close();
		return fractal;

	}

	public static void fractalToFile(EscapeTimeFractal fractal, File file) throws IOException {

		BufferedWriter out = new BufferedWriter(new FileWriter(file));
		Type type = fractal.getType();

		switch(type) {

		case JULIA:

			out.write(Type.JULIA.toString());
			break;

		case MANDELBROT:

			out.write(Type.MANDELBROT.toString());
			break;

		case NEWTON:
			
			out.write(Type.NEWTON.toString());
			break;
			
		default:

			out.write(Type.JULIA.toString());
			break;

		}
		
		out.newLine();
		out.write(Integer.toString(fractal.getIterations()) + "," + Double.toString(fractal.getThreshold()));
		out.newLine();
		out.write(Integer.toString(fractal.getWidth()) + "," + Integer.toString(fractal.getHeight()));
		out.newLine();
		out.write(Double.toString(fractal.getMinX()) + "," + Double.toString(fractal.getMaxX()) + "," + Double.toString(fractal.getMinY()) + "," + Double.toString(fractal.getMaxY()));
		out.newLine();
		out.write(Double.toString(fractal.getZoomIncrement()) + "," + Double.toString(fractal.getMoveIncrement()));
		out.newLine();
		Color[] color = fractal.getColor();
		
		for(int i=0;i<color.length;i++) {
			
			out.write(Integer.toString(color[i].getRGB()));
			
			if(i < color.length - 1) {
				
				out.write(",");
				
			}
			
		}

		out.newLine();
		ComplexNumber C;
		
		switch(type) {
		
		case JULIA:
			
			C = ((Julia) fractal).getC();
			out.write(C.getReal() + "," + C.getImaginary());
			break;
			
		case MANDELBROT:
			
			C = ((Mandelbrot) fractal).getZ();
			out.write(C.getReal() + "," + C.getImaginary());
			break;
			
		case NEWTON:
			
			C = ((Newton) fractal).getN();
			ComplexNumber alpha = ((Newton) fractal).getAlpha();
			out.write(C.getReal() + "," + C.getImaginary() + "," + alpha.getReal() + "," + alpha.getImaginary());
			break;
			
		default:
			
			C = ((Julia) fractal).getC();
			out.write(C.getReal() + "," + C.getImaginary());
			break;
		
		}
		
		out.close();

	}

}