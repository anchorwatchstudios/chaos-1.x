package com.anchorwatchstudios.software.science.chaos.fractal;

import java.io.File;

public class Document {
	
	private File file;
	private boolean isNew;
	private boolean isEdited;
	
	public Document() {
		
		file = new File("untitled.frc");
		isNew = true;
		isEdited = false;
		
	}
	
	public Document(File file) {
		
		this.file = file;
		isNew = false;
		isEdited = false;
		
	}
	
	public boolean isNew() {
		
		return isNew;
		
	}
	
	public boolean isEdited() {
		
		return isEdited;
		
	}
	
	public void setEdited(boolean isEdited) {
		
		this.isEdited = isEdited;
		
	}
	
	public String getName() {
		
		return file.getName();
		
	}
	
	public File getFile() {
		
		return file;
		
	}
	
}