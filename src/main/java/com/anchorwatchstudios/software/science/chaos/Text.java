package com.anchorwatchstudios.software.science.chaos;

public class Text {
	
	public static final String TITLE = "Chaos";
	public static final String DEFAULT_DIRECTORY = System.getProperty("user.home");
	public static final String BLANK_MESSAGE = "<html></html>";
	public static final String RENDER_MESSAGE = "Rendering...";
	public static final String OPEN_MESSAGE = "Opening file...";
	public static final String SAVE_MESSAGE = "Saving file...";
	public static final String EXPORT_MESSAGE = "Exporting image...";
	public static final String EXPORT_TITLE = "Export";
	public static final String OVERWRITE_MESSAGE = "The selected file already exists. Overwrite?";
	public static final String OVERWRITE_MESSAGE_TITLE = "Overwrite File";
	public static final String OPEN_ERROR_MESSAGE = "The file could not be opened because it is invalid or corrupt.";
	public static final String SAVE_ERROR_MESSAGE = "An error occured while trying to save the file.";
	public static final String EXPORT_ERROR_MESSAGE = "An error occured while trying to export the image.";
	public static final String ERROR_MESSAGE_TITLE = "An Error Occured";
	public static final String UNSAVED_MESSAGE = "Modifications have been made. Save changes?";
	public static final String ABOUT_MESSAGE = "<html><center><b>Chaos " + Chaos.VERSION_MAJOR + "." + Chaos.VERSION_MINOR + "." + Chaos.VERSION_PATCH + "<br>&copy 2015</b><br>AnchorWatch Studios</center></html>";
	public static final String ABOUT_MESSAGE_TITLE = "About Chaos...";
	public static final String MENU_TEXT_FILE = "File";
	public static final String MENU_TEXT_EDIT = "Edit";
	public static final String MENU_TEXT_VIEW = "View";
	public static final String MENU_TEXT_HELP = "Help";
	public static final String MENU_TEXT_ABOUT = "About...";
	public static final String MENU_TEXT_NEW = "New...";
	public static final String MENU_TEXT_NEW_JULIA = "Julia Set";
	public static final String MENU_TEXT_NEW_MANDELBROT = "Mandelbrot Set";
	public static final String MENU_TEXT_NEW_NEWTON = "Newton Basin";
	public static final String MENU_TEXT_OPEN = "Open...";
	public static final String MENU_TEXT_SAVE = "Save";
	public static final String MENU_TEXT_SAVEAS = "Save As...";
	public static final String MENU_TEXT_EXPORT_IMAGE = "Export Image...";
	public static final String MENU_TEXT_EXIT = "Exit";
	public static final String MENU_TEXT_ZOOM_IN = "Zoom In";
	public static final String MENU_TEXT_ZOOM_OUT = "Zoom Out";
	public static final String MENU_TEXT_MOVE_UP = "Move Up";
	public static final String MENU_TEXT_MOVE_DOWN = "Move Down";
	public static final String MENU_TEXT_MOVE_LEFT = "Move Left";
	public static final String MENU_TEXT_MOVE_RIGHT = "Move Right";
	public static final String MENU_TEXT_EDIT_SETTINGS = "Settings...";
	public static final String FILE_EXT_FRC = ".frc";
	public static final String FILE_EXT_PNG = ".png";
	public static final String FILE_DESC_FRC = "(.frc) Chaos Fractal";
	public static final String FILE_DESC_PNG = "(.png) Portable Network Graphic";
	public static final String DOC_EDITED_SYMBOL = "*";
	public static final String BUTTON_OK = "OK";
	public static final String BUTTON_CANCEL = "Cancel";
	public static final String BUTTON_APPLY = "Apply";
	public static final String MINX = "Min. X";
	public static final String MAXX = "Max. X";
	public static final String MINY = "Min. Y";
	public static final String MAXY = "Max. Y";
	public static final String ZOOM_INCREMENT = "Zoom Increment (%)";
	public static final String MOVE_INCREMENT = "Move Increment (%)";
	public static final String SCALE = "Scale";
	public static final String DIMENSIONS = "Dimensions";
	public static final String WIDTH = "Width";
	public static final String HEIGHT = "Height";
	public static final String COLOR = "Color";
	public static final String COLOR1 = "Color 1";
	public static final String COLOR2 = "Color 2";
	public static final String ITERATIONS = "Iterations";
	public static final String THRESHOLD = "Threshold";
	public static final String FRACTAL = "Fractal";
	public static final String JULIA_EQUATION = "(Z = Z^2 + C)";
	public static final String MANDELBROT_EQUATION = "(Z = Z^2 + C)";
	public static final String NEWTON_EQUATION = "(Z = Z - A(1 + (N - 1)Z^N) / (NZ^N-1)";
	public static final String ANTIALIAS = "Anti-aliasing";
	
	private Text() {
		
	}

}