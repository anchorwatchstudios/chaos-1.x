package com.anchorwatchstudios.software.science.chaos.fractal;

import java.awt.Color;
import java.awt.image.BufferedImage;

import com.anchorwatchstudios.barracuda.math.Calc;
import com.anchorwatchstudios.barracuda.math.ComplexNumber;

public class Newton extends EscapeTimeFractal {

	public static final ComplexNumber DEFAULT_N = new ComplexNumber(3, 0);
	public static final ComplexNumber DEFAULT_ALPHA = new ComplexNumber(1, 0);
	public static final double DEFAULT_THRESHOLD = .05;
	public static final double DEFAULT_MINX = -1.5;
	public static final double DEFAULT_MAXX = 1.5;
	public static final double DEFAULT_MINY = -1.5;
	public static final double DEFAULT_MAXY = 1.5;

	private ComplexNumber N;
	private ComplexNumber alpha;

	public Newton() {

		N = DEFAULT_N;
		alpha = DEFAULT_ALPHA;
		setType(Type.NEWTON);
		setThreshold(DEFAULT_THRESHOLD);
		setMinX(DEFAULT_MINX);
		setMaxX(DEFAULT_MAXX);
		setMinY(DEFAULT_MINY);
		setMaxY(DEFAULT_MAXY);
		setAntiAlias(DEFAULT_ANTIALIAS);

	}

	@Override
	public BufferedImage render() {

		BufferedImage image = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
		Color[] color = getColor();

		for(int i=0;i<image.getWidth();i++) {

			for(int j=0;j<image.getHeight();j++) {

				double a = (double) i * (getMaxX() - getMinX()) / (double) image.getWidth() + getMinX();		
				double b = (double) j * (getMaxY() - getMinY()) / (double) image.getHeight() + getMinY();
				ComplexNumber z = new ComplexNumber(a,b);

				double count = 0;
				ComplexNumber one = new ComplexNumber(1, 0);

				for(int k=0;k<getIterations();k++) {

					ComplexNumber numerator = z.pow((int) N.getReal());
					numerator = numerator.multiply(N.subtract(one));
					numerator = numerator.add(one);
					numerator = numerator.multiply(alpha);
					ComplexNumber denominator = z.pow((int) N.subtract(one).getReal());
					denominator = denominator.multiply(N);
					ComplexNumber oldZ = z;
					z = numerator.divide(denominator);

					if(z.subtract(oldZ).abs() > Math.pow(getThreshold(), 2.0)) {

						count++;

					}
					else {

						if(getAntiAlias()) {
							//TODO BROKEN
							count = getPreciseIterations(oldZ, getIterations() - count);

						}

						break;

					}

				}

				double value = count;
				Color pixelColor = color[0];

				if(value < getIterations()) {

					//pixelColor = colorRoots(value, z);
					pixelColor = colorAbs(value);

				}
				else {

					pixelColor = color[0];

				}

				image.setRGB(i, j, pixelColor.getRGB());

			}

		}

		return image;

	}

	public ComplexNumber getN() {

		return N;

	}

	public void setN(ComplexNumber C) {

		this.N = C;

	}

	public ComplexNumber getAlpha() {

		return alpha;

	}

	public void setAlpha(ComplexNumber alpha) {

		this.alpha = alpha;

	}

	private double getPreciseIterations(ComplexNumber c, double iterations) {
		//TODO BROKEN
		double log_zn = Math.log(c.abs()) / N.getReal();
		double nu = Math.log( log_zn / Math.log(N.getReal()) ) / Math.log(N.getReal());
		return iterations + 1 - nu;

	}

	@SuppressWarnings("unused")
	private double isInSet(ComplexNumber z) {

		double count = 0;
		ComplexNumber one = new ComplexNumber(1, 0);

		for(int i=0;i<getIterations();i++) {

			ComplexNumber numerator = z.pow((int) N.getReal());
			numerator = numerator.multiply(N.subtract(one));
			numerator = numerator.add(one);
			numerator = numerator.multiply(alpha);
			ComplexNumber denominator = z.pow((int) N.subtract(one).getReal());
			denominator = denominator.multiply(N);
			ComplexNumber oldZ = z;
			z = numerator.divide(denominator);

			if(z.subtract(oldZ).abs() > Math.pow(getThreshold(), 2.0)) {

				count++;

			}
			else {

				if(getAntiAlias()) {
					//TODO BROKEN
					count = getPreciseIterations(oldZ, getIterations() - count);

				}

				break;

			}

		}

		return count;

	}

	@SuppressWarnings("unused")
	private Color colorRoots(double value, ComplexNumber z) {

		Color pixelColor = getColor()[0];
		ComplexNumber r1 = new ComplexNumber(1, 0);
		ComplexNumber r2 = new ComplexNumber(-0.5, Math.sin(2*Math.PI/3));
		ComplexNumber r3 = new ComplexNumber(-0.5, -Math.sin(2*Math.PI/3));
		int col = (int) Math.floor(((double) value / (double) getIterations()) * (double) 255);

		if(getAntiAlias()) {

			double percentRed = z.subtract(r1).abs() / Math.pow(getThreshold(), 2);
			double percentGreen = z.subtract(r2).abs() / Math.pow(getThreshold(), 2);
			double percentBlue = z.subtract(r3).abs() / Math.pow(getThreshold(), 2);

			if(z.subtract(r1).abs() < Math.pow(getThreshold(), 2)) {

				int red = Calc.interpolate(Color.BLACK.getRed(), Color.RED.getRed(), percentRed);
				pixelColor = new Color(red, 0, 0);

			}
			if(z.subtract(r2).abs() < Math.pow(getThreshold(), 2)) {

				int green = Calc.interpolate(Color.BLACK.getGreen(), Color.RED.getGreen(), percentGreen);
				pixelColor = new Color(0, green, 0);

			}
			if(z.subtract(r3).abs() < Math.pow(getThreshold(), 2)) {

				int blue = Calc.interpolate(Color.BLACK.getBlue(), Color.RED.getBlue(), percentBlue);
				pixelColor = new Color(0, 0, blue);

			}

		}
		else {

			//Four roots
			if(z.getReal() > 1 - getThreshold() && z.getReal() < 1 + getThreshold()) {

				pixelColor = new Color(col, 0, 0);

			}
			if(z.getReal() < -1 + getThreshold() && z.getReal() > -1 - getThreshold()) {

				pixelColor = new Color(0, col, 0);

			}
			if(z.getImaginary() > 1 - getThreshold() && z.getImaginary() < 1 + getThreshold()) {

				pixelColor = new Color(0, 0, col);

			}
			if(z.getImaginary() < -1 + getThreshold() && z.getImaginary() > -1 - getThreshold()) {

				pixelColor = new Color(col, 0, col);

			}
			
			/*//Three Roots
			if(z.subtract(r1).abs() < Math.pow(getThreshold(), 2)) {

				pixelColor = new Color(col, 0, 0);

			}
			if(z.subtract(r2).abs() < Math.pow(getThreshold(), 2)) {

				pixelColor = new Color(0, col, 0);

			}
			if(z.subtract(r3).abs() < Math.pow(getThreshold(), 2)) {

				pixelColor = new Color(0, 0, col);

			}
			*/

		}

		return pixelColor;

	}

	private Color colorAbs(double value) {

		Color[] color = getColor();
		Color pixelColor;
		int red1 = (int) Math.floor(((double) value / (double) getIterations()) * (double) color[1].getRed());
		int green1 = (int) Math.floor(((double) value / (double) getIterations()) * (double) color[1].getGreen());
		int blue1 = (int) Math.floor(((double) value / (double) getIterations()) * (double) color[1].getBlue());
		Color color1 = new Color(color[1].getAlpha() <<24 | red1 <<16 | green1 <<8 | blue1);

		if(getAntiAlias()) {
			//TODO BROKEN
			int red2 = (int) Math.floor(((double) (value + 1) / (double) getIterations()) * (double) color[1].getRed());
			int green2 = (int) Math.floor(((double) (value + 1) / (double) getIterations()) * (double) color[1].getGreen());
			int blue2 = (int) Math.floor(((double) (value + 1) / (double) getIterations()) * (double) color[1].getBlue());
			Color color2 = new Color(color[1].getAlpha() <<24 | red2 <<16 | green2 <<8 | blue2);

			int red = Calc.interpolate(color2.getRed(), color1.getRed(), value / getIterations());
			int green = Calc.interpolate(color2.getGreen(), color1.getGreen(), value / getIterations());
			int blue = Calc.interpolate(color2.getBlue(), color1.getBlue(), value / getIterations());
			pixelColor = new Color(red, green, blue);

		}
		else {

			pixelColor = color1;

		}

		return pixelColor;

	}

}