package com.anchorwatchstudios.software.science.chaos;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

public class ImagePanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private int scale;
	private int xPos;
	private int yPos;
	private BufferedImage image;
	
	public ImagePanel() {
		
		scale = 0;
		xPos = 0;
		yPos = 0;
		image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
		
	}
	
	public BufferedImage getImage() {
		
		return image;
		
	}
	
	public void setImage(BufferedImage image) {
		
		this.image = image;
		
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		update();
		g.drawImage(image, xPos, yPos, scale, scale, null);
		g.setColor(Color.WHITE);
		
	}
			
	private void update() {
		
		int width = getWidth();
		int height = getHeight();
		
		if(width > height) {
			
			scale = height;
			
		}
		else {
			
			scale = width;
			
		}
		
		xPos = (width - scale)/2;
		yPos = (height - scale)/2;
		
	}
	
}